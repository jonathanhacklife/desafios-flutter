﻿import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

/// Guarda los datos en SharedPreferences en formato STRING
Future<void> saveData(tag, value) async {
  final pref = await SharedPreferences.getInstance();
  pref.setString(tag, jsonEncode(value));
}

/// Eliminar historial sharedpreferences
Future<void> deleteShared(tag) async {
  final pref = await SharedPreferences.getInstance();
  pref.remove(tag);
  print("$tag eliminado.");
}

Future<dynamic> getShared(String tag) async {
  final pref = await SharedPreferences.getInstance();
  print("SHARED");
  return json.decode(pref.getString(tag)!);
}
