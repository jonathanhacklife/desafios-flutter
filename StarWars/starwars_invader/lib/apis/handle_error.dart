﻿String handleError(respuesta) {
  switch (respuesta.statusCode) {
    case 400:
      throw '${respuesta.body.toString()}: ${respuesta.statusCode}';
    case 401:
      throw 'Las credenciales no son válidas';
    case 403:
      throw '${respuesta.body.toString()}: ${respuesta.statusCode}';
    case 404:
      throw '${respuesta.body.toString()}: ${respuesta.statusCode}';
    case 500:
      throw 'Error interno del servidor: ${respuesta.statusCode}';
    default:
      throw '${respuesta.body.toString()}: ${respuesta.statusCode}';
  }
}
