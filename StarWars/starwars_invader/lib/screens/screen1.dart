﻿import 'package:flutter/material.dart';
import 'package:starwars_invader/blocs/personajes_bloc.dart';
import 'package:starwars_invader/models/personaje_model.dart';
import 'package:starwars_invader/screens/screen2.dart';
import 'package:starwars_invader/styles/styles.dart';
import 'package:starwars_invader/blocs/online_switch.dart';
import 'package:starwars_invader/utils/shared.dart';
import 'package:starwars_invader/widgets/grid_lines.dart';
import 'package:starwars_invader/widgets/partial_border.dart';

final switchBLoC = SwitchBLoC();
final personajesBLoC = PersonajesBLoC();

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Title',
      theme: ThemeData(
        brightness: Brightness.light,
        /* light theme settings */
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        textTheme: swTextTheme,
        /* dark theme settings */
      ),
      themeMode: ThemeMode.dark,
      /* ThemeMode.system to follow system theme,
         ThemeMode.light for light theme,
         ThemeMode.dark for dark theme
      */
      home: const MyHomePage(title: 'StarWars Invaders'),
      routes: {
        '/screen1': (context) => MyApp(),
        '/screen2': (context) => Screen2(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<dynamic>(
      future: switchBLoC.checkDeviceConnection(),
      builder: (context, deviceConnection) {
        if (deviceConnection.hasData) {
          return StreamBuilder<dynamic>(
              stream: switchBLoC.getStreamConnectionMode,
              initialData: deviceConnection.data,
              builder: (context, internetConnection) {
                return Scaffold(
                  appBar: AppBar(title: Text(widget.title), backgroundColor: internetConnection.data ? backgroundColor : danger.withOpacity(0.5)),
                  drawer: MenuLateral(),
                  body: Container(
                    decoration: BoxDecoration(image: backgroundDecorationImage),
                    child: SingleChildScrollView(
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              // FEATURE: Animación ListView Silver
                              Text('Personajes', style: Theme.of(context).textTheme.displaySmall),
                              Text('Seleccione un personaje para ver más información', style: Theme.of(context).textTheme.bodyText1),
                              // FIXME: Mejorar la interfaz
                              FutureBuilder<dynamic>(
                                future: internetConnection.data ? personajesBLoC.getPersonajes() : getShared("swapiDB"),
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    return ListView.builder(
                                        itemCount: 10,
                                        shrinkWrap: true,
                                        physics: const NeverScrollableScrollPhysics(),
                                        itemBuilder: (buildContext, index) {
                                          return Column(
                                            crossAxisAlignment: CrossAxisAlignment.stretch,
                                            children: [
                                              SizedBox(height: 20),
                                              CardPersonaje(personaje: Swapi.fromJson(snapshot.data).results?[index], indice: index, color: internetConnection.data ? secondary : danger),
                                            ],
                                          );
                                        });
                                  } else if (snapshot.hasError) {
                                    // TODO: Permitir utilizar la aplicación en modo OFFLINE
                                    return Text("${snapshot.error}");
                                  }
                                  return CircularProgressIndicator();
                                },
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              });
        } else {
          return Scaffold(
            backgroundColor: backgroundColor,
            appBar: AppBar(title: Text(widget.title)),
            drawer: const MenuLateral(),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Conectando al sistema...', style: Theme.of(context).textTheme.headline4),
                  CircularProgressIndicator(),
                ],
              ),
            ),
          );
        }
      },
    );
  }
}

class CardPersonaje extends StatelessWidget {
  const CardPersonaje({Key? key, required this.personaje, required this.indice, required this.color}) : super(key: key);
  final dynamic personaje;
  final int indice;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return PartialBorderContainer(
      gradient: LinearGradient(colors: const [tertiary, tertiary]),
      onPressed: () {},
      strokeWidth: 2,
      padding: 10,
      child: Container(
        color: color.withOpacity(0.3),
        child: CustomPaint(
          painter: BackgroundPaint(),
          child: ListTile(
            onTap: () => Navigator.pushNamed(context, '/screen2', arguments: {"person": personaje, "indx": indice}),
            leading: CircleAvatar(
              backgroundColor: color,
              maxRadius: 30,
              child: Text(
                "${personaje.vehicles.length} ${personaje.starships.length}",
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            title: Text(personaje.name, style: Theme.of(context).textTheme.headline6),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Altura: ${personaje.height}m'),
                Text('Peso: ${personaje.mass}kg'),
                Text('Género: ${personaje.gender}'),
              ],
            ),
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            trailing: Icon(Icons.chevron_right_rounded, size: 30),
          ),
        ),
      ),
    );
  }
}

class MenuLateral extends StatefulWidget {
  const MenuLateral({Key? key}) : super(key: key);

  @override
  State<MenuLateral> createState() => _MenuLateralState();
}

class _MenuLateralState extends State<MenuLateral> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Expanded(
            child: FutureBuilder<dynamic>(
              future: switchBLoC.checkDeviceConnection(),
              builder: (context, deviceConnection) {
                if (deviceConnection.hasData) {
                  return StreamBuilder<dynamic>(
                      stream: switchBLoC.getStreamConnectionMode,
                      initialData: deviceConnection.data,
                      builder: (context, connectionMode) {
                        return ListView(
                          children: [
                            DrawerHeader(
                              decoration: BoxDecoration(color: connectionMode.data ? dark : Colors.black),
                              child: Center(child: Text(connectionMode.data ? 'Conectado' : 'Desconectado', style: TextStyle(color: tertiary))),
                            ),
                            ListTile(
                              title: Text("Cambiar a modo ${connectionMode.data ? 'desconectado' : 'en línea'}"),
                              // BUG: Cuando se cambia a modo desconectado, el switch permanece en el modo inicial
                              trailing: Switch(
                                value: connectionMode.data,
                                onChanged: (_) {},
                              ),
                              onTap: () => switchBLoC.setOnlineMode(!connectionMode.data),
                            )
                          ],
                        );
                      });
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
